# E2E Cypress Challenge

This project was mainly based in cypress for testexecution, cucumber for BDD integration and sequelize for database manipulation

Why Cucumber?

* Upside:
  * 1) Since we are talking about e2e testing, it would be nice to have tests written in an ubiquetous language that everyone in the team could understand and whose reports are in a friendly format
  * 2)  If the steps are created in an organized manner, it is possible to reuse them and the next tests are faster to create as most of the steps will already be created

* Downside:
  * 1) it takes more time to create the first tests as there is the overhead of common code creation
  * 2) it requires to use the Page Object pattern [which currently generates some discussion](https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/)

Why sequelize?

I want to insert data into the database that will be necessary throughtout the test run

## Installation

This project requires node to run. Node can be found [here](https://nodejs.org/)

In order to run the tests we need to install its dependencies first. In the root folder ./voyager-mars-e2e-test-cucumber execute the following command :

```sh
npm install
```

Now we can run our end to end tests (assuming we have the server up and running as described [here](../README.md) in section Voyager Mars Rover Application). Should you want to run the tests along with DB setup before the test execution and cucumber report generation after the test execution (as we would do in a CI tool like Jenkins, Circle CI, Github Actions, etc.), in a command line type:

```sh
npm run e2e  #runs the test in Electron in headless mode
npm run e2e:chrome  #runs the test in Chrome
npm run e2e:edge  #runs the test in Microsoft Edge
```

Before the test execution, test data will be inserted into the database. Reason being that we want to have specific and controlled data for test automation execution so the enviroment can be shared with other users

After the test execution, cypress will generate .mp4 files inside './cypress/videos'

After the test execution, cucumber will generate a report inside './cypress/cucumber/reports/e2e.html' as well as open it in the default browser. It will look like this one:

![Report Example](readme/Cucumber_Report.PNG)

## Project Structure

* In **cypress/integration** we place our '.feature' files. Those files are written in Gherkin (BDD) and will be mapped by cocumber to javascript code, in this way our tests are readable to non developers and the report is very friendly
* In **cypress/support/step_definitions** is where we map the steps used by cucumber to actual javascript code. Steps usually are as short as possible because they are just a place for interfacing Gherking (BDD) to .js and nothing more than that
* In **cypress/pages** we place the code used by steps. These are the actual workers, they are the ones that know everything about the pages and how to interact with them. There is a lot of room for code improvement here like extracting some common interactions (for example the search bar that is available in all three pages) into a separated module
* In **cypress/db** we place db related code: models, connection and a setup script. Here we should put more work in order to pick the connection details (such as user and password) from another source and not to be hardcoded. If we are using a CI tool such as Jenkins or Circle CI we could store credentials in them and give the credentials as enviroment variables. Also we could use a crypto vault to query these information

## Next scenarios to be tested

The following automated tests would be the next ones to be added:

* Delete plateau that already have rovers associated to the company
* Reduce the plateau size so a Rover would become out of bounds

## Improvements and Bugs in Voyager application

Checkout some potential issues and some sugested improvements:

* [General comments](readme/COMMENTS.md#general)
* [AC 1. Define the plateau size by company like described on plateau](readme/COMMENTS.md#ac1)
* [AC2. Create, update and delete the company's rovers](readme/COMMENTS.md#ac2)