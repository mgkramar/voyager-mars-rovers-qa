# BUS AND IMPROMENTS

## GENERAL

### General Bugs

> * **Bug**: Some input fields such as the Upper X Position when creating a new Plateau fields do not have length checking neither in the UI nor in the backend. The message displayed to the user is an internal exception do to the database not being able to process an insert with the out of range value.
> * **Action**: Every and each input field should have a defined maximum value/length and this check should be done in both the front end app and in the back end
![Exception being leadked](ExceptionLeakage.png)



> * **Bug**: Some characters (such as chinese and russian) are not supported by the application but when we try to use one of these we get a database exception being 'leaked' to the frontend.
> * **Action**: **1)** if business requirements do not support Chinese characters then both frontend and backend should not allow it to get to the database and a proper message should be displayed to the user. Currently a database exception is shown
        **2)** if business requirements do support Chinese characters then the backend should be refactored as such that the database supports Chinese characters. That will need to include a review of the front end to make sure the characters are properly encoded when sent to the backend

### General Improvements

> * **Issue**: most of the HTML elements do not have id attributes. This makes the automation code written less robust to changes and harder to read. 
> * **Action**: In the [Cypress Best Practices](https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements) page, it is recommended adding attributes to element that are indented to cypress to identify

## AC1

### AC1 Improvements

> * **Issue**: When we create a new plateau, the only information shown to select a company is its code. However, when we see the plateau in the Plateau page the only information displayed is the company name. That makes it hard to instantly match Plateau with a company just created from an user point of view. How can we sure that the company code select is the correct one? 
> * **Action**: show both information in the Plateau page
![Exception being leadked](Company1.png)
![Exception being leadked](Company2.png)

## AC2

### AC2 Bugs

> * **Bug**: Hovers can have the same position. You can create a new rover for a company with the same (X, Y) coordinates and the system allows you too. One even can move one a hover onto a (X,Y) position that is already occupied.
> * **Action**: 	
>   * **1) The business allow said behavior** -> for which I would recommend to at least show a message on the screen asking for confirmation because the two hovers might clash 	
>   * **2) The business does not allow said behavior** -> for which I would recommend to add checks in the 'Create New' and 'Move action' to confirm that the (X, Y) positions won´t clash

![Exception being leadked](Clash.png)