const cucumber = require("cypress-cucumber-preprocessor").default;
const dbSetup = require("../db/setup/setup")


module.exports = (on, config) => {
  on("file:preprocessor", cucumber());

  on("task", {
    "setup:db": () => {
        return dbSetup();
    },
  });
};
