const { DataTypes, Model } = require("sequelize");
const sequelize = require("../connection");
class Company extends Model {}

Company.init(
  {
    id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true },
    created_at: { type: DataTypes.DATE },
    updated_at: { type: DataTypes.DATE },
    code: { type: DataTypes.STRING(255) },
    name: { type: DataTypes.STRING(255) },
    description: { type: DataTypes.STRING(255) },
    email: { type: DataTypes.STRING(255) },
  },
  {
    sequelize,
    modelName: "Company",
    timestamps: false,
  }
);

module.exports = Company;
