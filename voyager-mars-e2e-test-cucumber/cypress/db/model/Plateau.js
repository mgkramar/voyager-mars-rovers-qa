const { DataTypes, Model } = require("sequelize");
const sequelize = require("../connection")
class Plateau extends Model {}

Plateau.init(
  {
    id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true },
    created_at: { type: DataTypes.DATE },
    updated_at: { type: DataTypes.DATE },
    code: { type: DataTypes.STRING(255) },
    name: { type: DataTypes.STRING(255) },
    upper_x_position: { type: DataTypes.INTEGER },
    upper_y_position: { type: DataTypes.INTEGER },
    id_company: { type: DataTypes.INTEGER.UNSIGNED },
  },
  {
    sequelize,
    modelName: "Plateau",
    timestamps: false,
  }
);

module.exports = Plateau;
