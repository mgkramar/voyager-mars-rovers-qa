const Plateau = require("../model/Plateau");
const Company = require("../model/Company");

async function dbSetup() {
  const company1 = {
    created_at: "2020-10-25 17:54:34",
    updated_at: "2020-10-25 17:54:34",
    code: "TA_COMPANY001",
    name: "TA - COMPANY NAME 001",
    description: "áéíóúç - ÁÉÍÓÚÇ",
    email: "ta_company001@test.com",
  };

  upsert(company1, Company);

  const company2 = {
    created_at: "2020-10-25 17:54:34",
    updated_at: "2020-10-25 17:54:34",
    code: "TA_COMPANY002",
    name: "TA - COMPANY NAME 002",
    description: "æÆøØåÅ",
    email: "ta_company002@test.com",
  };

  upsert(company2, Company);

  const company3 = {
    created_at: "2020-10-25 17:54:34",
    updated_at: "2020-10-25 17:54:34",
    code: "TA_COMPANY003",
    name: "TA - COMPANY NAME 003",
    description: "0123_3210+=",
    email: "ta_company003@test.com",
  };

  upsert(company3, Company);

  const company4 = {
    created_at: "2020-10-25 17:54:34",
    updated_at: "2020-10-25 17:54:34",
    code: "TA_COMPANY004",
    name: "TA - COMPANY NAME 004",
    description: "THE cOmPaNy ###",
    email: "ta_company004@test.com",
  };

    const createdCompany = await upsert(company4, Company);

    const company5 = {
      created_at: "2020-10-25 17:54:34",
      updated_at: "2020-10-25 17:54:34",
      code: "TA_COMPANY005",
      name: "TA - COMPANY NAME 005",
      description: "COMPANY INC.",
      email: "ta_company005@test.com",
    };


    upsert(company5, Company);

  const plateauToInsert = {
    created_at: "2020-10-25 17:54:34",
    updated_at: "2020-10-25 17:54:34",
    code: "TA_PLATEAU",
    name: "TA - PLATEAU FOR ROVERS TESTING",
    upper_x_position: 0,
    upper_y_position: 2,
    id_company: createdCompany.get("id"),
  };

  upsert(plateauToInsert, Plateau);

  return null;
}

function upsert(objToInsert, model) {
  return model
    .findOne({ where: { code: objToInsert["code"] } })
    .then(async (foundObj) => {
      if (foundObj) {
        await foundObj.update(objToInsert);
      } else {
        await model.create(objToInsert);
      }
      return await model.findOne({ where: { code: objToInsert["code"] } });
    });
}

module.exports = dbSetup;
