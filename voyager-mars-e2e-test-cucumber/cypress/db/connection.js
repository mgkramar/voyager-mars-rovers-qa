const { Sequelize } = require("sequelize");

module.exports = new Sequelize(`marsrover`, `mars`, `rover`, {
  port: 3306,
  host: `localhost`,
  dialect: "mysql",
});
