import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const NavigationPanel = require("../../pages/NavigationPanel");
const navigationPanel = new NavigationPanel();

Given(`I navigated to the Voyager App Portal`, () => {
  navigationPanel.navigateTo("/");
});

When(`I select {string} menu option in the Navigation Pane`, (menuName) => {
  navigationPanel.selectMenuOption(menuName);
});

Then(`the navigation panel should be visible`, () => {
  navigationPanel.panelShouldBeVisible();
});

Then(`I should see {string} displayed as the page title`, (pageTitle) => {
  navigationPanel.getPageTitle().should("eq", pageTitle);
});
