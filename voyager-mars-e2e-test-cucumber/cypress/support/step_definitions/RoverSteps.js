import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const RoversPage = require("../../pages/RoversPage");
const roversPage = new RoversPage();

When("I create a Rover with the following characteristics:", (roverData) => {
  roversPage.createRoverFromDataTable(roverData);
});

When(
  "I edit the Rover form with the following characteristics:",
  (roverData) => {
    roversPage.editRoverFromDataTable(roverData);
  }
);

When(
  "I click in the {string} action for Rover with code {string}",
  (action, roverCode) => {
    roversPage.clickInRoverAction(action, roverCode);
  }
);

When("I send the {string} command to the rover", (command) => {
  roversPage.sendCommand(command);
});

Then("I should see an error message {string}", (message) => {
  roversPage.verifyMessageAppears(message);
});

Then(
  "I should see that the Rover with code {string} has the following information:",
  (roverCode, roverData) => {
    roversPage.verifyRoverIsPresent(roverCode, roverData);
  }
);

Then("I should not see a Rover with code {string}", (roverCode) => {
  roversPage.verifyRoverIsNotPresent(roverCode);
});
