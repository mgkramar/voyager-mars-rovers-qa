import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const PlateausPage = require("../../pages/PlateausPage");
const plateausPage = new PlateausPage();

When(
  "I create a Plateau with the following characteristics:",
  (plateauData) => {
    plateausPage.createPlateauFromDataTable(plateauData);
  }
);

When(`I search a Plateau by {string}`, (searchText) => {
  plateausPage.searchPlateau(searchText);
});

When(`I delete the result #{int} in the Plateau list`, (resultNumber) => {
  plateausPage.deleteNthPlateau(resultNumber);
});

Then(
  "I should see the following Plateau in the Plateau list:",
  (plateauData) => {
    plateausPage.verifyPlateauResults(plateauData);
  }
);

Then("I should see that the New Plateau dialog dissapeared", () => {
  plateausPage.verifyThatDialogDisspeared();
});

Then(
  `I should see {string} being displayed in the footer`,
  (expectedNumberOfResults) => {
    plateausPage.verifyThatNumberOfResultsIs(expectedNumberOfResults);
  }
);

Then("I should see {string} message in the Plateus list", (message) => {
  plateausPage.verifyPlateauMessage(message);
});
