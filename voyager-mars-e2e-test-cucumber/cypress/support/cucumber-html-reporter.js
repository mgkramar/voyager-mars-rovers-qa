const reporter = require("cucumber-html-reporter");

const options = {
  theme: "hierarchy",
  jsonDir: "cypress/cucumber/json",
  output: "cypress/cucumber/reports/e2e.html",
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  ignoreBadJsonFile: true,
  scenarioTimestamp: true,
  metadata: {
    "App Version": "1.0.0",
    "Test Environment": "LOCAL",
    Browser: "Chrome  54.0.2840.98",
    Platform: "Windows 10",
    Parallel: "Scenarios",
    Executed: "Locally",
  },
};

reporter.generate(options);
