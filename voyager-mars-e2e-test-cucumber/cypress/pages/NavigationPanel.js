const BasePage = require("./BasePage.js");

export default class NavigationPanel extends BasePage {
  navigationMenuOptions = {
    COMPANIES: "a[href='/company/admin']",
    PLATEAUS: "a[href='/plateau/admin']",
    ROVERS: "a[href='/rover/admin']",
  };

  panel = ".v-navigation-drawer__content";

  selectMenuOption(menuOptionName) {
    const menuOption = this.navigationMenuOptions[menuOptionName.toUpperCase()];
    if (!menuOption) {
      throw new Error(
        `Menu option [${menuOptionName}] does not exist in the ${this.pageName()} `
      );
    }
    cy.get(menuOption).click();

    return this;
  }

  panelShouldBeVisible() {
    cy.get(this.panel).should("be.visible");
  }

  pageName() {
    return "Navigation Panel";
  }
}
