export default class SimplePage {
  baseUrl = "http://localhost:8080";

  navigateTo(path) {
    cy.visit(this.baseUrl + path);
    return this;
  }

  getPageTitle() {
    return cy.title();
  }
}
