const BasePage = require("./BasePage.js");

export default class RoversPage extends BasePage {
  CREATE_NEW_BUTTON_TEXT = "Create New";
  SAVE_BUTTON_TEXT = "Save";
  ROVER_DIALOG = ".rovers-form";

  TABLE_ROWS = "tbody tr";
  CODE_COLLUMNS = "tbody tr td:nth-of-type(1)";
  CODE_COLUMN = "td:nth-of-type(1)";
  NAME_COLUMN = "td:nth-of-type(2)";
  X_POSITION_COLUMN = "td:nth-of-type(3)";
  Y_POSITION_COLUMN = "td:nth-of-type(4)";
  CARDINAL_DIRECTION_COLUMN = "td:nth-of-type(5)";
  COMPANY_COLUMN = "td:nth-of-type(6)";
  ACTION_COLUMN = "td:nth-of-type(7)";

  ROVER_DIALOG = ".rover-form";
  CODE_LABEL = "Code";
  NAME_LABEL = "Name";
  X_POSITION_LABEL = "X Position";
  Y_POSITION_LABEL = "Y Position";
  CARDINAL_DIRECTION_LABEL = "Cardinal Direction";
  COMPANY_LABEL = "Company";

  MOVE_DIALOG = ".move-form";
  MOVE_INPUT = "*[placeHolder *= 'Put your instruction here']";
  MOVE_BUTTON_TEXT = "Move";

  STATUS_MESSAGE = "*[role= 'status']";
  STATUS_CLOSE = "Close";

  ACTION_ICONS = {
    EDIT: "button:nth-of-type(1)",
    MOVE: "button:nth-of-type(2)",
    DELETE: "button:nth-of-type(3)",
  };

  createRoverFromDataTable(roverDataTable) {
    roverDataTable.rawTable.forEach((row) => {
      const rover = this.createRoverObjectFromDataTableRow(row);
      this.createRover(rover);
    });
  }

  editRoverFromDataTable(roverDataTable) {
    const rover = this.createRoverObjectFromDataTableRow(
      roverDataTable.rawTable[0]
    );
    this.editRover(rover);
  }

  createRoverObjectFromDataTableRow(row) {
    return {
      code: row[0],
      name: row[1],
      xPosition: row[2],
      yPosition: row[3],
      cardinalDirection: row[4],
      company: row[5],
    };
  }

  createRover(rover) {
    cy.contains(this.CREATE_NEW_BUTTON_TEXT).click();
    this.fillRoverInformation(rover);
    cy.get(this.ROVER_DIALOG).contains(this.SAVE_BUTTON_TEXT).click();
  }

  editRover(rover) {
    this.fillRoverInformation(rover);
    cy.get(this.ROVER_DIALOG).contains(this.SAVE_BUTTON_TEXT).click();
  }

  fillRoverInformation(rover) {
    this.fillDialogInput(this.CODE_LABEL, rover["code"]);
    this.fillDialogInput(this.NAME_LABEL, rover["name"]);
    this.fillDialogInput(this.X_POSITION_LABEL, rover["xPosition"]);
    this.fillDialogInput(this.Y_POSITION_LABEL, rover["yPosition"]);
    this.fillDialogInput(
      this.CARDINAL_DIRECTION_LABEL,
      rover["cardinalDirection"]
    );

    cy.get(this.ROVER_DIALOG)
      .contains(this.COMPANY_LABEL)
      .siblings("div:nth-of-type(1)")
      .click();

    for (let i = 0; i < 3; i++) {
      cy.get(".v-menu__content").scrollTo("bottom");
      //Usually we avoid waiting fixed amount of time, but this select is really tricky
      cy.wait(500);
    }

    cy.get(".v-menu__content").contains(rover["company"]).click();
  }

  fillDialogInput(inputLable, valueToInput) {
    cy.get(this.ROVER_DIALOG)
      .contains(inputLable)
      .siblings("input")
      .clear()
      .type(valueToInput);
  }

  searchRover(searchText) {
    cy.get(this.SEARCH_INPUT).clear().type(searchText).type("{enter}");
  }

  verifyThatNumberOfResultsIs(expectedNumberOfResults) {
    cy.get(this.PAGINATION_LABEL).should("to.be", expectedNumberOfResults);
  }

  verifyRoverIsPresent(roverCode, roverDataTable) {
    const roverInformation = this.createRoverObjectFromDataTableRow(
      roverDataTable.rawTable[0]
    );

    cy.get(this.CODE_COLLUMNS)
      .contains(roverCode)
      .parent()
      .then(($row) => {
        cy.wrap($row)
          .find(this.NAME_COLUMN)
          .should("to.contain", roverInformation["name"]);

        cy.wrap($row)
          .find(this.X_POSITION_COLUMN)
          .should("to.contain", roverInformation["xPosition"]);

        cy.wrap($row)
          .find(this.Y_POSITION_COLUMN)
          .should("to.contain", roverInformation["yPosition"]);

        cy.wrap($row)
          .find(this.CARDINAL_DIRECTION_COLUMN)
          .should("to.contain", roverInformation["cardinalDirection"]);

        cy.wrap($row)
          .find(this.COMPANY_COLUMN)
          .should("to.contain", roverInformation["company"]);
      });
  }

  verifyRoverIsNotPresent(roverCode) {
    cy.get(this.CODE_COLLUMNS).contains(roverCode).should("not.to.exist");
  }

  clickInRoverAction(action, roverCode) {
    cy.get(this.CODE_COLLUMNS)
      .contains(roverCode)
      .parent()
      .find(this.ACTION_COLUMN)
      .find(this.ACTION_ICONS[action.toUpperCase()])
      .click();
  }

  sendCommand(command) {
    cy.get(this.MOVE_DIALOG).find(this.MOVE_INPUT).clear().type(command);
    cy.get(this.MOVE_DIALOG)
      .find("button:nth-of-type(2)")
      .click({ force: true });
  }

  verifyMessageAppears(message) {
    cy.get(this.STATUS_MESSAGE).contains(message).should("be.visible");
    cy.get(this.STATUS_MESSAGE).contains(this.STATUS_CLOSE).click();
  }

  verifyThatDialogDisspeared() {
    cy.get(this.ROVER_DIALOG, { timeout: 10000 }).should("not.exist");
  }
}
