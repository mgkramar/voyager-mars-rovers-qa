const BasePage = require("./BasePage.js");

export default class PlateausPage extends BasePage {
  CREATE_NEW_BUTTON_TEXT = "Create New";
  SAVE_BUTTON_TEXT = "Save";
  PAGINATION_LABEL = ".v-data-footer__pagination";
  SEARCH_INPUT = ".container .v-text-field__slot input";
  DELETE_BUTTON = ".mdi-delete";
  TABLE_ROWS = "tbody tr";

  PLATEAU_DIALOG = ".plateau-form";
  CODE_LABEL = "Code";
  NAME_LABEL = "Name";
  UPPER_X_POSITION_LABEL = "Upper X Position";
  UPPER_Y_POSITION_LABEL = "Upper Y Position";
  COMPANY_LABEL = "Company";

  createPlateauFromDataTable(plateauDataTable) {
    plateauDataTable.rawTable.forEach((row) => {
      const plateau = this.createPlateauObjectFromDataTableRow(row);
      this.createPlateau(plateau);
    });
  }

  createPlateauObjectFromDataTableRow(row) {
    return {
      code: row[0],
      name: row[1],
      upperXPosition: row[2],
      upperYPosition: row[3],
      company: row[4],
    };
  }

  createPlateau(plateau) {
    cy.contains(this.CREATE_NEW_BUTTON_TEXT).click();
    this.fillPlateauInformation(plateau);
    cy.get(this.PLATEAU_DIALOG).contains(this.SAVE_BUTTON_TEXT).click();
  }

  fillPlateauInformation(plateau) {
    this.fillDialogInput(this.CODE_LABEL, plateau["code"]);
    this.fillDialogInput(this.NAME_LABEL, plateau["name"]);
    this.fillDialogInput(
      this.UPPER_X_POSITION_LABEL,
      plateau["upperXPosition"]
    );
    this.fillDialogInput(
      this.UPPER_Y_POSITION_LABEL,
      plateau["upperYPosition"]
    );

    cy.get(this.PLATEAU_DIALOG)
      .contains(this.COMPANY_LABEL)
      .siblings("div:nth-of-type(1)")
      .click();

    for (let i = 0; i < 3; i++) {
      cy.get(".v-menu__content").scrollTo("bottom");
      //Usually we avoid waiting fixed amount of time, but this select is really tricky
      cy.wait(500);
    }

    cy.get(".v-menu__content").contains(plateau["company"]).click();
  }

  fillDialogInput(inputLable, valueToInput) {
    cy.get(this.PLATEAU_DIALOG)
      .contains(inputLable)
      .siblings("input")
      .type(valueToInput);
  }

  searchPlateau(searchText) {
    cy.get(this.SEARCH_INPUT).clear().type(searchText).type("{enter}");
  }

  verifyThatNumberOfResultsIs(expectedNumberOfResults) {
    cy.get(this.PAGINATION_LABEL).should("to.be", expectedNumberOfResults);
  }

  deleteNthPlateau(resultNumber) {
    cy.get(this.TABLE_ROWS)
      .eq(resultNumber - 1)
      .find(this.DELETE_BUTTON)
      .click();
  }

  verifyPlateauResults(plateauDataTable) {
    const plateauToFind = this.createPlateauObjectFromDataTableRow(
      plateauDataTable.rawTable[0]
    );

    cy.get("tbody tr:nth-of-type(1)").then(($element) => {
      cy.wrap($element)
        .find("td:nth-of-type(1)")
        .should("contain", plateauToFind["code"]);
      cy.wrap($element)
        .find("td:nth-of-type(2)")
        .should("contain", plateauToFind["name"]);
      cy.wrap($element)
        .find("td:nth-of-type(3)")
        .should("contain", plateauToFind["upperXPosition"]);
      cy.wrap($element)
        .find("td:nth-of-type(4)")
        .should("contain", plateauToFind["upperYPosition"]);
      cy.wrap($element)
        .find("td:nth-of-type(5)")
        .should("contain", plateauToFind["company"]);
    });
  }

  verifyPlateauMessage(message) {
    cy.get("td").contains(message).should("be.visible");
  }

  verifyThatDialogDisspeared() {
    cy.get(this.PLATEAU_DIALOG, { timeout: 10000 }).should("not.exist");
  }
}
