Feature: Plateau Management
    Scenario Outline: The Plateu module should allow creation of different Plateus including characters from languages other than English
        Given I navigated to the Voyager App Portal
        When I select 'Plateaus' menu option in the Navigation Pane
        When I create a Plateau with the following characteristics:
            | <Code> | <Name> | <UpperXPosition> | <UpperYPosition> | <CompanyCode> |
        Then I should see that the New Plateau dialog dissapeared

        Examples:
            | Code   | Name                            | UpperXPosition | UpperYPosition | CompanyCode   |
            | TA1234 | TA - Johan Bach Müller Straße   | 888            | 0              | TA_COMPANY001 |
            | TA5555 | TA - Jane O'Donnel              | 0              | 777            | TA_COMPANY002 |
            | TA0    | TA - João Venâncio Caixa d'água | 999            | 999            | TA_COMPANY003 |

    Scenario Outline: The Plateu module should allow user to search for any information in the Plateu in a case insensitive manner
        When I search a Plateau by <SearchText>
        Then I should see '1-1 of 1' being displayed in the footer
        Then I should see the following Plateau in the Plateau list:
            | <Code> | <Name> | <UpperXPosition> | <UpperYPosition> | <CompanyName> |

        Examples:
            | SearchText | Code | Name                            | UpperXPosition | UpperYPosition | CompanyName           |
            | '555'      | 5555 | TA - Jane O'Donnel              | 0              | 777            | TA - COMPANY NAME 002 |
            | 'mü'       | 1234 | TA - Johan Bach Müller Straße   | 888            | 0              | TA - COMPANY NAME 001 |
            | 'D\'ÁGUA'  | 0    | TA - João Venâncio Caixa d'água | 999            | 999            | TA - COMPANY NAME 003 |
            | '777'      | 5555 | TA - Jane O'Donnel              | 0              | 777            | TA - COMPANY NAME 002 |
            | '888'      | 1234 | TA - Johan Bach Müller Straße   | 888            | 0              | TA - COMPANY NAME 001 |

    Scenario Outline: The Plateu module should allow user to delete a Plateau
        When I search a Plateau by <SearchText>
        When I delete the result #1 in the Plateau list
        Then I should see '-' being displayed in the footer
        Then I should see 'No matching records found' message in the Plateus list

        Examples:
            | SearchText |
            | '555'      |
            | 'straße'   |
            | 'D\'ÁGUA'  |