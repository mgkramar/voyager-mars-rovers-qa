Feature: Rover Management
    Scenario: A Rover should not be created to a company that does not have a plateau assigned to it
        Given I navigated to the Voyager App Portal
        When I select 'Rovers' menu option in the Navigation Pane
        When I create a Rover with the following characteristics:
            | TA_NO_COMP | TA - The Machine 1 | 0 | 0 | S | TA_COMPANY005 |
        Then I should see an error message ' There isn\'t a Plateau created to this company'


    Scenario: A Rover should not be created if its (X, Y) positions are out of its Plateau boundaries
        When I edit the Rover form with the following characteristics:
            | TA_OUT_BOUND | TA - The Machine 2 | 1 | 0 | N | TA_COMPANY004 |
        Then I should see an error message 'wasn\'t created/updated because it\'s position (x or y) value exceeded plateau'

    Scenario: The Rover Module should allow a Rover to be added to a company that has a plateau assigned to it
        When I edit the Rover form with the following characteristics:
            | TA_VALID | TA - The Machine 3 | 0 | 0 | N | TA_COMPANY004 |
        Then I should see that the Rover with code 'TA_VALID' has the following information:
            | TA_VALID | TA - The Machine 3 | 0 | 0 | N | TA - COMPANY NAME 004 |

    Scenario: The Rover Module should allow a Rover to be edited
        When I click in the 'Edit' action for Rover with code 'TA_VALID'
        When I edit the Rover form with the following characteristics:
            | TA_EDIT | TA - The Machine 3 Edited | 0 | 0 | S | TA_COMPANY004 |
        Then I should see that the Rover with code 'TA_EDIT' has the following information:
            | TA_EDIT | TA - The Machine 3 Edited | 0 | 0 | S | TA - COMPANY NAME 004 |

    Scenario: The Rover Module should not allow a Rover to be moved to a position outside of its Plateau boundaries
        When I click in the 'Move' action for Rover with code 'TA_EDIT'
        When I send the 'RM' command to the rover
        Then I should see an error message 'went out of plateau boundaries'

    Scenario: The Rover Module should allow a Rover to be moved to a position inside of its Plateau boundaries
        When I click in the 'Move' action for Rover with code 'TA_EDIT'
        When I send the 'RRRRRRMMRRM' command to the rover
        Then I should see that the Rover with code 'TA_EDIT' has the following information:
            | TA_EDIT | TA - The Machine 3 Edited | 0 | 1 | S | TA - COMPANY NAME 004 |

    Scenario: The Rover Module should allow a Rover to be deleted
        When I click in the 'Delete' action for Rover with code 'TA_EDIT'
        Then I should not see a Rover with code 'TA_EDIT'