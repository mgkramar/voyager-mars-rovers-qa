Feature: Smoke test

    Scenario: The navigation panel should be visible upon visiting the Voyager App Portal
        Given I navigated to the Voyager App Portal
        Then I should see 'Voyager' displayed as the page title
        Then the navigation panel should be visible